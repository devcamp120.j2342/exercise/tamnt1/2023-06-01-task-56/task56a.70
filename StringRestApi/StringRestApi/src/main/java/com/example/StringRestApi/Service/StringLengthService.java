﻿package com.example.StringRestApi.Service;

import org.springframework.stereotype.Service;

@Service
public class StringLengthService {

    public int getStringLength(String string) {
        return string.length();
    }
}
