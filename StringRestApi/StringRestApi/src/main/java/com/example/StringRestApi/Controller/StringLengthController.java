﻿package com.example.StringRestApi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StringRestApi.Service.StringLengthService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class StringLengthController {
    @Autowired
    private StringLengthService service;

    @GetMapping("/length")

    public int getLength(@RequestParam String string) {
        return service.getStringLength(string);
    }
}
